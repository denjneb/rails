#!/bin/sh
{
  echo "# Added by consistency.sh"
  echo "USER_NAME=`id -un`"
  echo "HOST_GID=`id -g $USER`"
  echo "HOST_UID=`id -u $USER`"
} >> .env
