# Environment construction procedure

## Define a workspace to execute Docker commands
```shell
$ cd ./docker
```

## Place the environment file
```shell
$ mv -f "${SOURCE_PATH}" "./.env"
```

## Set the container user with the same information as the HOST user for consistency
```shell
$ ./consistency.sh
```
When executed the HOST user information will be output to `file: ./docker/.env` as below.
```shell
# Added by consistency.sh
USER_NAME=tonystark
HOST_UID=1000
HOST_GID=1000
```

## Initial build
```shell
$ make init
```

## Optionally cache each mount points
`file: ./docker/docker-compose.yml`  
Add volume option (`:cached`) each mount points of HOST and container.

## Synchronize defined CLIENT packages from HOST
Uncomment as below.
`file: ./docker/client/Dockerfile`
```dockerfile
COPY ${FROM}/package*.json $TO
```

## Set up database config for API
`file: ./project/api/config/database.yml`
```yaml
default: &default
  # Set values to each keys as below.
  username: <%= ENV['MYSQL_USER_NAME'] %>
  password: <%= ENV['MYSQL_ROOT_PASSWORD'] %>
  host: <%= ENV['MYSQL_HOST_NAME'] %>
```
```shell
$ make db
```

## Rebuild images, and start up container
```shell
$ make buildup
```

## Access
API - Rails: http://localhost:3000/  
CLIENT - React: http://localhost:8000/
